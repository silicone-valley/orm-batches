# Doing Batches using ORM (this is an Attempt)
The aim here is to find an optimal way to do batches using ORM/JPA,
This project consists of the direct JDBC approach and using JPA (Hibernate 5 implementation).
This project uses an in-memory-database which may skew the results a little.

WARNING: do not increase the job size too far beyond 200 000 items, this may crash your computer

## Minimum Requirements

- Maven (3.4)
- JDK (1.8)

## Download
```
$ git clone https://gitlab.com/silicone-valley/orm-batches.git
```

## How to run it

```bash
# build project
$ mvn clean package

# JPA Job
$ java -jar target/orm-batches-1.0.0-SNAPSHOT.jar jpa 1000

# JDBC Job
$ java -jar target/orm-batches-1.0.0-SNAPSHOT.jar jdbc 1000
```
Lookout for the following log file entry
```
INFO: ############# jpa processed 1000 items and took 0.4 seconds #########
```
