package com.siliconvalley;

import com.siliconvalley.entity.Download;
import com.siliconvalley.entity.Track;
import com.siliconvalley.util.FakeUtil;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hmhlongo
 */
public class BatchCLI {

    private static final Logger L = Logger.getLogger(BatchCLI.class.getName());
    private static final Path WORKING_DIR = Paths.get(System.getProperty("user.home"), "orm-batch");
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:" + WORKING_DIR.resolve("itunes-stats");
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "";

    private EntityManagerFactory factory;
    private final int jobSize;
    private final String jobType;

    public BatchCLI(String[] args) {
        if (args.length < 2) {
            L.warning("Missing arguments");
            L.warning("Usage: BatchCLI [jpa|jdbc] <number_of_items>");
            System.exit(1);
        }

        if (!(args[0].equalsIgnoreCase("jdbc") || args[0].equalsIgnoreCase("jpa"))) {
            L.warning("Invalid persistence type");
            L.warning("Usage: BatchCLI [jpa|jdbc] <number_of_items>");
            System.exit(1);
        }

        this.jobType = args[0];
        this.jobSize = Integer.parseInt(args[1]);
    }

    private void createDB() throws IOException {
        clearDBFiles();
        Map props = new HashMap<>();
        props.put("javax.persistence.jdbc.driver", DB_DRIVER);
        props.put("javax.persistence.jdbc.url", DB_URL);
        props.put("javax.persistence.jdbc.user", DB_USER);
        props.put("javax.persistence.jdbc.password", DB_PASSWORD);

        factory = Persistence.createEntityManagerFactory("silicon-pu", props);
    }

    private Job createJob() {
        if (jobType.equalsIgnoreCase("jdbc")) {
            return new JdbcJob(prepareBatch(this.jobSize), DB_DRIVER, DB_URL, DB_USER, DB_PASSWORD);
        } else {
            return new JpaJob(createEntityManager(), prepareBatch(this.jobSize));
        }
    }

    private EntityManager createEntityManager() {
        return factory.createEntityManager();
    }

    private static void clearDBFiles() throws IOException {

        if (!Files.exists(WORKING_DIR)) {
            // Nothing to clear
            return;
        }
        Iterator<Path> it = Files.list(WORKING_DIR).iterator();
        while (it.hasNext()) {
            Path p = it.next();
            if (!Files.isDirectory(p)) {
                L.log(Level.INFO, "Deleting db file {0}...", p);
                Files.delete(p);
            }
        }
    }

    private List<Download> prepareBatch(int jobSize) {
        List<Download> items = new ArrayList<>();
        for (int i = 0; i < jobSize; i++) {
            Download dl = new Download();
            dl.setTrack(new Track(1L));
            dl.setUser(FakeUtil.generateUser());
            dl.setUserAgent(FakeUtil.generateUserAgent());
            items.add(dl);
        }

        return items;
    }

    private void close() {
        this.factory.close();
    }

    public static void main(String[] args) throws Exception {

        BatchCLI cli = new BatchCLI(args);

        cli.createDB();

        Job job = cli.createJob();

        long start = System.currentTimeMillis();

        int total = job.execute();
        float latency = (System.currentTimeMillis() - start) / 1000f;

        L.info(String.format("############# %s processed %s items and took %s seconds #########", cli.jobType, total, latency));

        job.dumpResults();

        cli.close();
    }
}
