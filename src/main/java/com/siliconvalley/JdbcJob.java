package com.siliconvalley;

import com.siliconvalley.entity.Download;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hmhlongo
 */
public class JdbcJob implements Job {

    private static final Logger L = Logger.getLogger(JdbcJob.class.getName());
    private final List<Download> items;
    private final String driver;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPass;

    public JdbcJob(List<Download> items, String driver, String dbUrl, String dbUser, String dbPass) {
        this.items = items;
        this.driver = driver;
        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
        this.dbPass = dbPass;
    }

    @Override
    public void dumpResults() {
        L.warning("Dumping NOT implemented!!!");
    }

    @Override
    public int execute() throws SQLException {
        try (Connection conn = getDBConnection()) {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO download (dated, modified, user_agent, track_id, uzer_id) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?, ?)");
            for (Download dl : items) {
                stmt.setString(1, dl.getUserAgent());
                stmt.setLong(2, dl.getTrack().getId());
                stmt.setLong(3, dl.getUser().getId());
                stmt.addBatch();
            }
            int[] ids = stmt.executeBatch();
            return ids.length;
        }
    }

    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            L.log(Level.SEVERE, e.getMessage(), e);
        }
        try {
            dbConnection = DriverManager.getConnection(dbUrl, dbUser, dbPass);
            return dbConnection;
        } catch (SQLException e) {
            L.log(Level.SEVERE, e.getMessage(), e);
        }
        return dbConnection;
    }

}
