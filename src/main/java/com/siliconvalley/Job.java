package com.siliconvalley;

/**
 *
 * @author hmhlongo
 */
public interface Job {

    int BATCH_SIZE = 300;

    int execute() throws Exception;

    void dumpResults();
}
