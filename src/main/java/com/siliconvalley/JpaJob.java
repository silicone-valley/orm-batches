package com.siliconvalley;

import com.siliconvalley.entity.Download;
import com.siliconvalley.entity.Track;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author hmhlongo
 */
public class JpaJob implements Job {

    private static final Logger L = Logger.getLogger(JdbcJob.class.getName());

    private final EntityManager em;
    private final List<Download> items;

    public JpaJob(EntityManager em, List<Download> items) {
        this.em = em;
        this.items = items;
    }

    @Override
    public void dumpResults() {
        L.warning("Dumping NOT implemented!!!");
    }

    @Override
    public int execute() throws Exception {

        em.getTransaction().begin();
        Track track = em.find(Track.class, 1L);

        for (Download dl : items) {
            dl.setDated(LocalDateTime.now());
            dl.setModified(dl.getDated());
        }

        track.setDownloads(items);

        em.getTransaction().commit();
//
//        Long total = em.createQuery("SELECT COUNT(d) FROM Download d", Long.class)
//                .getSingleResult();
//
//        System.out.println(" TOTAL: " + total);

        return track.getDownloads().size();
    }
}
