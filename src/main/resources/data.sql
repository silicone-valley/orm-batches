INSERT INTO uzer (id, dated, modified, username, email, first_name, last_name) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'masterjack', 'master@hotmail', 'Hlulani', 'Mhlongo');
INSERT INTO uzer (id, dated, modified, username, email, first_name, last_name) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'les', 'les@hotmail', 'Lesley', 'Netshiavhela');
INSERT INTO uzer (id, dated, modified, username, email, first_name, last_name) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'readyfred', 'readyfred@hotmail', 'Freddy', 'Malatsi');
INSERT INTO uzer (id, dated, modified, username, email, first_name, last_name) VALUES (4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'tmusodz', 'tmusodz@hotmail', 'Trevor', 'Musodza');
INSERT INTO uzer (id, dated, modified, username, email, first_name, last_name) VALUES (5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'khumbs', 'khumbs@hotmail', 'Khumbulani', 'Ncube');

INSERT INTO track (id, dated, modified, artist, title, album, release_date) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Tupac', 'Dear Mama', 'Me Against The World', '1995-05-05');
